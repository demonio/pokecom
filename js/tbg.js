$(document).on('click', '.modal-backdrop', function (event) {
    bootbox.hideAll()
});
function register() {
bootbox.dialog({
                title: "Registrace",
                message: '<div class="row">  ' +
                    '<div class="col-md-12"> ' +
                    '<form class="form-horizontal" id="register" method="post" action="javascript:register_post();void(0);" enctype="multipart/form-data"> ' +
                    '<div class="input-group">' +
	'<span class="input-group-addon" id="basic-addon1">Herní přezdívka</span>' +
	'<input type="text" name="username" class="form-control" required>' +
	'</div>' +
	'<br>'+
	'<div class="input-group">' +
	'<span class="input-group-addon" id="basic-addon1">Heslo</span>' +
	'<input type="password" name="password" class="form-control" required>' +
	'</div>' +
	'<br>'+
	'<div class="input-group">' +
	'<span class="input-group-addon" id="basic-addon1">Ověření hesla</span>' +
	'<input type="password" name="password2" class="form-control" required>' +
	'</div>' +
	'<br>'+
        '<div class="input-group">' +
        '<span class="input-group-addon" id="basic-addon1">Email</span>' +
        '<input type="text" name="email" class="form-control" required>' +
        '</div>' +
	'<br>'+
        '<div class="input-group">' +
        '<span class="input-group-addon" id="basic-addon1">Facebook url</span>' +
        '<input type="text" name="fb" class="form-control">' +
        '</div>' +
        '<br>'+	
        '<div class="input-group">' +
        '<span class="input-group-addon" id="basic-addon1">Město</span>' +
        '<input type="text" name="mesto" class="form-control">' +
        '</div>' +
        '<br>'+
        '<div class="input-group">' +
        '<span class="input-group-addon" id="basic-addon1">Městská část</span>' +
        '<input type="text" name="cast" class="form-control">' +
        '</div>' +
        '<br>'+
        '<div class="input-group">' +
        '<span class="input-group-addon" id="basic-addon1">Team</span>' +
	'<select name="team" class="form-control" id="basic-addon1">' +
	'<option value="valor">Valor</option>' +
	'<option value="mystic">Mystic</option>' +
	'<option value="instinct">Instinct</option>' +
	'</select>' +
        '</div>' +
        '<br>'+
	'<button type="submit" class="btn btn-success btn-lg">Odeslat</button>' +
                    '</form> </div>  </div>',
            });
         }
         
function login() {
bootbox.dialog({
                title: "Prihlaseni",
                message: '<div class="row">  ' +
                    '<div class="col-md-12"> ' +
                    '<form class="form-horizontal" id="login" method="post" action="javascript:login_post();void(0);" enctype="multipart/form-data"> ' +
                    '<div class="input-group">' +
	  	 						'<span class="input-group-addon" id="basic-addon1">Herní přezdívka</span>' +
	    						'<input type="text" name="username" class="form-control" required>' +
	  					  '</div>' +
	  					  '<br>'+
	  					  '<div class="input-group">' +
	  	 						'<span class="input-group-addon" id="basic-addon1">Heslo</span>' +
	    						'<input type="password" name="password" class="form-control" required>' +
	  					  '</div>' +
	  					  '<br>'+
	  					  '<button type="submit" class="btn btn-success btn-lg">Odeslat</button>' +
                    '</form> </div>  </div>',
            });
         }
         
function login_post(){

    var url = "php/login.php"; // the script where you handle the form input.

    $.ajax({
           type: "POST",
           url: url,
           data: $("#login").serialize(), // serializes the form's elements.
           success: function(data)
           {
               $('#content').html(data);
               bootbox.hideAll()
           }
         });

    return false; // avoid to execute the actual submit of the form.
}
function register_post(){

    var url = "php/register.php"; // the script where you handle the form input.

    $.ajax({
           type: "POST",
           url: url,
           data: $("#register").serialize(), // serializes the form's elements.
           success: function(data)
           {
               $('#content').html(data);
               bootbox.hideAll()
           }
         });

    return false; // avoid to execute the actual submit of the form.
}
function logout() {
	$.ajax({url: "php/logout.php", 
		success: function(data)
           {
               $('#content').html(data);
           }});
}
function redirect(where) {
	$.ajax({url: "php/"+where+".php", 
		success: function(data)
           {
               $('#content').html(data);
           }});
}
function get_users(arg) {
        $.ajax({url: "php/get_users.php?arg="+arg, 
                success: function(data)
           {
               $('#users').html(data);
           }});
}
function user_info(arg) {
        $.ajax({url: "php/user_info.php?id="+arg, 
                success: function(data)
           {
	       bootbox.dialog({
		   title: "Informace",
		   message: data
	       });
           }});
}
function get_page(arg) {
        $.ajax({url: "php/"+arg+".php", 
                success: function(data)
           {
               $('#obsah').html(data);
           }});
}
function map() {
	$.ajax({url: "php/map.php", 
		success: function(data)
           {
               $('#obsah').html(data);
           }});
}
function get_chat() {
	$.ajax({url: "php/get_chat.php", 
		success: function(data)
           {
               $('#messages').html(data);
           }});
}

function isEmpty(str) {
    return (!str || 0 === str.length);
}

function isBlank(str) {
    return (!str || /^\s*$/.test(str));
}

function send_chat() {
    var msg = document.getElementById("msg").value
    if (!isEmpty(msg)){
	$.ajax({url: "php/send_chat.php?msg="+msg, 
		success: function(data)
           {
               $('#messages').html(data);
          }});
    }
    document.getElementById("msg").value = ""
}

function upgrade(id, what) {
	$.ajax({url: "php/upgrade.php?type="+what+"&id="+id, 
		success: function(data)
           {
               $('#content').html(data);
           }});
}

function display_image(e){
bootbox.dialog({
                title: e.alt,
                message: "<img style='width:100%;' src='"+e.src+"'  alt='"+e.alt+"' >"
            });
}
