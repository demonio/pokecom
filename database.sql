CREATE DATABASE `pokemon`;
CREATE TABLE `users` (
  `id` int NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `username` char(64) COLLATE 'utf8_czech_ci' NOT NULL,
  `password` char(64) COLLATE 'utf8_czech_ci' NOT NULL,
  `fb` char(255) COLLATE 'utf8_czech_ci' NOT NULL,
  `mesto` char(64) COLLATE 'utf8_czech_ci' NOT NULL,
  `cast` char(64) COLLATE 'utf8_czech_ci' NOT NULL,
  `team` char(64) COLLATE 'utf8_czech_ci' NOT NULL
);
ALTER TABLE `users`
ADD UNIQUE `username` (`username`);
ALTER TABLE `users`
ADD `email` char(64) COLLATE 'utf8_czech_ci' NOT NULL AFTER `password`;
ALTER TABLE `users`
ADD `image` char(255) COLLATE 'utf8_czech_ci' NOT NULL;
CREATE TABLE `akce` (
  `id` int NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `user` int(11) NOT NULL,
  `date` char(64) NOT NULL,
  `nazev` char(64) NOT NULL,
  `popis` text(1024) NOT NULL,
  `misto` char(255) NOT NULL
);
CREATE TABLE `chat` (
  `id` int NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `user` int NOT NULL,
  `cas` char(64) NOT NULL,
  `zprava` varchar(512) NOT NULL
);
